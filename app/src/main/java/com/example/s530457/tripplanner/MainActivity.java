package com.example.s530457.tripplanner;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
    public void calculateTime(View v) throws NumberFormatException
    {
        try {
            TextView Distance = findViewById(R.id.Distance);
            double dis = Double.parseDouble(Distance.getText().toString());
            TextView Speed = findViewById(R.id.Speed);
            double speedinmiles = Double.parseDouble(Speed.getText().toString());
            TextView NoofBreaks = findViewById(R.id.NumOfBreaks);
            int numOfBreaks = Integer.parseInt(NoofBreaks.getText().toString());

            double travelledTime = dis / speedinmiles + (numOfBreaks * 15)/60;
            TextView TravelledTime = findViewById(R.id.TravelledTime);
            TravelledTime.setText("The time travelled is " + String.format("%.2f", travelledTime) + " hours");
        }
        catch(NumberFormatException e){
            TextView TravelledTime = findViewById(R.id.TravelledTime);
            TravelledTime.setText("The details entered are not numeric. Please enter valid details");
        }

    }
    public void instructions(View v) {
        TextView InstructionDetails = findViewById(R.id.InstructionDetails);
        InstructionDetails.setText("Please enter the distance travelled in miles, speed in miles/hour and number of 15 minutes break taken" +
                "Then click on button below to calculate time travelled");
    }

    public void reset(View v)
    {
    EditText Distance = findViewById(R.id.Distance);
    TextView Speed = findViewById(R.id.Speed);
    TextView NumOfBreaks = findViewById(R.id.NumOfBreaks);
    TextView Break = findViewById(R.id.BreakTime);
Distance.setText("Distance Travelled in miles");
Speed.setText("Speed of car in miles/hour ");
Break.setText("Enter break time in minutes");
NumOfBreaks.setText("Enter the total number of 15mins breaks");
    }


}
